package com.nipitpon.week8;

public class TestTree {
    private int x;
    private int y;
    private final int x_max = 19;
    private final int y_max = 19;

    public TestTree(int x, int y) {
        this.x = x;
        this.y = y;

        if (x > x_max) this.x = x_max;
        if (x < 0) this.x = 0;
        if (y > y_max) this.y = y_max;
        if (y < 0) this.y = 0;
    }

    public void print() {
        System.out.println("Position :");
        System.out.print("X : "+x);
        System.out.println(" Y : "+y);
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
