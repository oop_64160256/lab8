package com.nipitpon.week8;

import java.lang.Math;
public class TriangleShape {
    private double a;
    private double b;
    private double c;
    public TriangleShape(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double findArea() {
        double s = (a+b+c)/2;
        double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
        System.out.println("Find Area of Triangle");
        System.out.println("A : "+a);
        System.out.println("B : "+b);
        System.out.println("C : "+c);
        System.out.println("Area : "+area);
        return area;
    }

    public double findPerimeter() {
        double perimeter = a+b+c;
        System.out.println("Find Perimeter of Triangle");
        System.out.println("A : "+a);
        System.out.println("B : "+b);
        System.out.println("C : "+c);
        System.out.println("Perimeter : "+perimeter);
        return perimeter;
    }
}
