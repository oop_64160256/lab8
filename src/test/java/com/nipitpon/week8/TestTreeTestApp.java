package com.nipitpon.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTreeTestApp {

    @Test
    public void shouldSuccess1(){
        TestTree tree1 = new TestTree(5, 10);
        assertEquals(5, tree1.getX());
        assertEquals(10, tree1.getY());
    }

    @Test
    public void shouldSuccess2(){
        TestTree tree1 = new TestTree(0, 10);
        assertEquals(0, tree1.getX());
        assertEquals(10, tree1.getY());
    }

    @Test
    public void shouldSuccessWithNegativeNumber(){
        TestTree tree1 = new TestTree(-5, 10);
        assertEquals(0, tree1.getX());
        assertEquals(10, tree1.getY());
    }

    @Test
    public void shouldSuccessWithNegativeNumber2(){
        TestTree tree1 = new TestTree(-5, -11230);
        assertEquals(0, tree1.getX());
        assertEquals(0, tree1.getY());
    }

    @Test
    public void shouldSuccessWithOverNumber(){
        TestTree tree1 = new TestTree(20, 10);
        assertEquals(19, tree1.getX());
        assertEquals(10, tree1.getY());
    }

    @Test
    public void shouldSuccessWithOverNumber2(){
        TestTree tree1 = new TestTree(2, 1030);
        assertEquals(2, tree1.getX());
        assertEquals(19, tree1.getY());
    }

    @Test
    public void shouldSuccessWithOverNumber3(){
        TestTree tree1 = new TestTree(1230, 1030);
        assertEquals(19, tree1.getX());
        assertEquals(19, tree1.getY());
    }
    
}
